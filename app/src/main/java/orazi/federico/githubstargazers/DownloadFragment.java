package orazi.federico.githubstargazers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import orazi.federico.githubstargazers.model.Stargazer;

public class DownloadFragment extends Fragment {

    public interface DownloadListener {
        void onFinishDownload(List<Stargazer> stargazers);
    }

    private static final String BASE_URL = "https://api.github.com/repos";
    private static final String STARGAZERS_COMMAND = "/stargazers";
    private DownloadListener listener;
    private String ownerName;
    private String repoName;

    public DownloadFragment() { }

    public static DownloadFragment newInstance(String owner, String repo) {
        DownloadFragment fragment = new DownloadFragment();
        Bundle bundle = new Bundle();
        bundle.putString("owner", owner);
        bundle.putString("repo", repo);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof  DownloadListener) {
            listener = (DownloadListener) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if(bundle != null) {
            ownerName = bundle.getString("owner");
            repoName = bundle.getString("repo");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_download, container, false);

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        final List<Stargazer> stargazers = new ArrayList<>();
        final String url = BASE_URL + "/" + ownerName + "/" + repoName + STARGAZERS_COMMAND;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    Log.d("JsonArrayRequest", "Response ok from: " + url);
                    for(int i=0; i<response.length(); i++) {
                        JSONObject stargazer = response.getJSONObject(i);
                        String username = stargazer.getString("login");
                        String avatarURL = stargazer.getString("avatar_url");
                        stargazers.add(new Stargazer(username, avatarURL));
                    }
                    if (listener != null) {
                        listener.onFinishDownload(stargazers);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("JsonArrayRequest", "Error detected: " + error.getMessage());
                new AlertDialog.Builder(getActivity())
                        .setCancelable(false)
                        .setTitle(R.string.download_error_title)
                        .setMessage(R.string.download_error_text)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (listener != null) {
                                    listener.onFinishDownload(null);
                                }
                            }
                        })
                        .show();
            }
        });
        requestQueue.add(jsonArrayRequest);

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
