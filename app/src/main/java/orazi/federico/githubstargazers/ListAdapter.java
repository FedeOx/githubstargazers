package orazi.federico.githubstargazers;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import orazi.federico.githubstargazers.model.Stargazer;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private List<Stargazer> stargazersList;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgAvatar;
        public TextView txtUsername;

        public ViewHolder(View view) {
            super(view);
            imgAvatar = (ImageView) view.findViewById(R.id.img_avatar);
            txtUsername = (TextView) view.findViewById(R.id.txt_username);
        }
    }

    public ListAdapter(List<Stargazer> stargazersList) {
        this.stargazersList = stargazersList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Stargazer user = stargazersList.get(position);
        new DownloadImageTask(holder.imgAvatar).execute(user.getAvatarURL());
        holder.txtUsername.setText(user.getUsername());
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.imgAvatar.setImageResource(0);
    }

    @Override
    public int getItemCount() {
        return stargazersList.size();
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView imageView;

        public DownloadImageTask(ImageView imageView) {
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            String url = params[0];
            Bitmap icon = null;
            try {
                InputStream in = new URL(url).openStream();
                icon = BitmapFactory.decodeStream(in);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return icon;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if(bitmap != null) {
                imageView.setImageBitmap(bitmap);
            } else {
                imageView.setImageResource(R.drawable.ic_baseline_broken_image_24px);
            }
        }
    }
}
