package orazi.federico.githubstargazers.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Stargazer implements Parcelable {
    private String username;
    private String avatarURL;

    public Stargazer(String username, String avatarURL) {
        this.username = username;
        this.avatarURL = avatarURL;
    }

    protected Stargazer(Parcel in) {
        username = in.readString();
        avatarURL = in.readString();
    }

    public static final Creator<Stargazer> CREATOR = new Creator<Stargazer>() {
        @Override
        public Stargazer createFromParcel(Parcel in) {
            return new Stargazer(in);
        }

        @Override
        public Stargazer[] newArray(int size) {
            return new Stargazer[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(username);
        parcel.writeString(avatarURL);
    }

    public String getUsername() {
        return username;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }

    @Override
    public String toString() {
        return this.username + this.avatarURL;
    }
}
