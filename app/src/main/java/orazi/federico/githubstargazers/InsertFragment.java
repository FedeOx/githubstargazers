package orazi.federico.githubstargazers;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class InsertFragment extends Fragment {

    public interface InsertListener {
        void onExecuteClick(String ownerName, String repoName);
    }

    private InsertListener listener;

    private EditText edtOwner;
    private EditText edtRepo;
    private Button btnExecute;

    public InsertFragment() { }

    public static InsertFragment newInstance() {
        return new InsertFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof InsertListener) {
            listener = (InsertListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_insert, container, false);

        edtOwner = (EditText) view.findViewById(R.id.edt_name);
        edtRepo = (EditText) view.findViewById(R.id.edt_repo);
        btnExecute = (Button) view.findViewById(R.id.btn_execute);

        btnExecute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null) {
                    listener.onExecuteClick(edtOwner.getText().toString(), edtRepo.getText().toString());
                }
            }
        });

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
