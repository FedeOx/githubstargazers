package orazi.federico.githubstargazers;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import orazi.federico.githubstargazers.model.Stargazer;

public class MainActivity extends AppCompatActivity implements  DownloadFragment.DownloadListener, InsertFragment.InsertListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addFragment(InsertFragment.newInstance(), false);
    }

    @Override
    public void onBackPressed() {
        int fragmentNum = getSupportFragmentManager().getBackStackEntryCount();
        if(fragmentNum == 1) {
            getSupportActionBar().setTitle(R.string.app_name);
        }
        if (getSupportFragmentManager().getBackStackEntryCount() == 0){
            getSupportActionBar().setTitle(R.string.app_name);
            new AlertDialog.Builder(this)
                    .setTitle(R.string.dialog_back_pressed_title)
                    .setMessage(R.string.dialog_back_pressed)
                    .setPositiveButton(R.string.dialog_back_pressed_positive, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.dialog_back_pressed_negative, null)
                    .show();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onFinishDownload(List<Stargazer> stargazers) {
        getSupportFragmentManager().popBackStack();
        if(stargazers != null) {
            getSupportActionBar().setTitle(R.string.toolbar_result);
            replaceFragment(ListFragment.newInstance(stargazers), true);
        }
    }

    @Override
    public void onExecuteClick(String ownerName, String repoName) {
        replaceFragment(DownloadFragment.newInstance(ownerName, repoName), true);
    }

    private void addFragment(Fragment fragment, boolean back) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.fragment_container, fragment);
        if (back) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    private void replaceFragment(Fragment fragment, boolean back) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        if (back) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }
}
